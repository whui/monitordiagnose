#!/usr/bin/env python
# -*- coding: UTF-8 -*-
 
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
 
# python 2.3.*: email.Utils email.Encoders
from email.utils import COMMASPACE,formatdate
from email import encoders
 
import os
import smtplib 
 
#server['name'], server['user'], server['passwd']
def send_mail(subject, to): 
    assert type(to) == list 
 
    msg = MIMEMultipart() 
    msg['From'] = 'support@dianjoy.com' 
    msg['Subject'] = subject
    msg['To'] = COMMASPACE.join(to) #COMMASPACE==', ' 
    msg['Date'] = formatdate(localtime=True) 
    msg.attach(MIMEText('Alarm', 'html', 'utf-8')) 
 
    smtp = smtplib.SMTP('smtp.exmail.qq.com') 
    smtp.login('support@dianjoy.com', 'dianjoy123') 
    smtp.sendmail('support@dianjoy.com' ,  COMMASPACE.join(to), msg.as_string()) 
    smtp.close()

def send_mail_with_text(subject, to,  text): 
    assert type(to) == list 
 
    msg = MIMEMultipart() 
    msg['From'] = 'support@dianjoy.com' 
    msg['Subject'] = subject
    msg['To'] = COMMASPACE.join(to) #COMMASPACE==', ' 
    msg['Date'] = formatdate(localtime=True) 
    msg.attach(MIMEText(text, 'html', 'utf-8')) 
 
    smtp = smtplib.SMTP('smtp.exmail.qq.com') 
    smtp.login('support@dianjoy.com', 'dianjoy123') 
    smtp.sendmail('support@dianjoy.com' ,  COMMASPACE.join(to), msg.as_string()) 
    smtp.close()
