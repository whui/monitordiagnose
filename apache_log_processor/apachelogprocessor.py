#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import fileinput
import sys 
import re 
import time 
import os
from urlparse import urlparse
from apachelogparse import logline, log_to_dic, req_to_dic, rootpathcollection
from email_sender import send_mail

#检查字典中是否存在此项，如存在则计数加1，不存在则插入
def dic_add(dic, key):
    #no need to add empty key
    if key == '' :
        return 
    if dic.has_key(key):
        dic[key]+=1
    else:
        dic[key]=1

#由IP的字典和另一个计数器组成的类
class hostcount:
    ips = {}
    linecount = 1
    def __init__(self,ip):
        self.ips = {}
        self.ips[ip] = 1   
    def addip(self, ip):
        self.ips[ip] = 1   
        self.linecount = self.linecount +1 

#请求消耗时间的统计
class costcount:
    rootpath = ''
    hit = 0
    cost = 0
    min = 0
    max = 0
    def __init__(self,rootpath):
        self.rootpath = rootpath
        self.hit = 0
        self.cost = 0
        self.min = 0
        self.max = 0
    def add(self, cost):
        self.hit+=1
        self.cost+=int(cost)
        if cost>self.max:
            self.max=cost
        if cost<self.min:
            self.min=cost
    def avgtime(self):
        if self.hit>0:
            return self.cost/self.hit
        return 0

def dic_add_hostcount(dic, key,  host):
    if key == '':#no need to continue if app_id is empty
        return
    if dic.has_key(key):
        dic[key].addip(host)
    else:
        dic[key] = hostcount(host)

def dic_add_app_cid(dic, app_id, cid, host):
    tuplekey = (app_id, cid)
    if dic.has_key(tuplekey):
        dic[tuplekey].addip(host)
    else:
        dic[tuplekey] = hostcount(host)

#将给定的文件名（带路径）拆分成日期和小时
def split_fullfilepath_todatetime(line):
    filename = os.path.split(line)[1]#可以兼容不同OS的路径格式
    print filename
    filename = filename.replace('dianjoy.com-access_','')
    log_line_re = re.compile(r'''(?P<date>\d{4}-\d{2}-\d{2})#date
    -
    (?P<hour>\d{2})#hour
    .* #others
    ''', re.VERBOSE)
    m = log_line_re.match(filename)
    if m:
        groupdict = m.groupdict()
        return (groupdict['date'], groupdict['hour'])
    else:
        print "Input file name not match expected format "
        print "Expect:/opt/lampp/logs/dianjoy.com-access_2013-01-24-00"
        print "Actual:%s" % line

def initlog():
    import logging

    logger = logging.getLogger()
    code_file_path = os.path.abspath(os.path.dirname(sys.argv[0])) #代码文件所在目录
    hdlr = logging.FileHandler(os.path.join(code_file_path,'./apachelogprocessor%s.log' % time.strftime('%Y-%m-%d',time.localtime(time.time()))))#在文件所在目录建log文件，一天之内用同一个
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    
    console = logging.StreamHandler()
    logger.addHandler(console)

    return logger
    
def prn_obj(obj):
    print ', '.join(['%s:%s' % item for item in obj.__dict__.items()])
    
#主函数
syslog = initlog()
if len(sys.argv) <2:
    syslog.error('Argvs error,please input one arg: hourly apache log name(with path)')
    sys.exit()
#hourlog = 'C:\Users\HUI\Documents\python\TestData\dianjoy.com-access_2013-01-24-00-100000'
hourlog = sys.argv[1]
if not os.path.exists(hourlog):
    syslog.error('Input file %s not exist,please input valid log file' % hourlog)
    sys.exit()
    
T1 = time.time() #计时器
#从文件名获取日期和小时

datetimedic = split_fullfilepath_todatetime(hourlog)
currentdate = datetimedic[0]
currenthour = datetimedic[1]

#与数据库相关的四组统计数据
adlist_clicks = {} #t_adlist_click
mid_clicks = {} #t_mid_click_log
clicks = {} #t_click
dones = {} #t_downok

#其他重要统计数据
statuscount = {}
channels = {} #channel id stat
linecount = 0
rootpatchcount = {}#各页面数量统计
costcounts={}#请求消耗时间的统计

#TODO: 此处需重构，新建一个mid_log建立类来构造file对象，现在的写法太繁琐
adclick_midlog = hourlog+'-adlist_midlog'
click_midlog = hourlog+'-click_midlog'
midclick_midlog = hourlog+'-midclick_midlog'
downok_midlog = hourlog+'-downok_midlog'

connect_midlog = hourlog+'-connect_midlog'
give_midlog = hourlog+'-give_midlog'
spend_midlog = hourlog+'-spend_midlog'
adnotify_midlog = hourlog+'-adnotify_midlog'

getinstalled_midlog = hourlog+'-getinstalled_midlog'
syslog.info('begin to creat mid_log files')
f = open(adclick_midlog, "wb")  
f.truncate() #清空原有内容
f2 = open(click_midlog, "wb")  
f2.truncate() 
f3 = open(midclick_midlog, "wb")  
f3.truncate() 
f4 = open(downok_midlog, "wb")  
f4.truncate() 
f5 = open(connect_midlog, "wb")  
f5.truncate() 
f6 = open(give_midlog, "wb")  
f6.truncate() 
f7 = open(spend_midlog, "wb")  
f7.truncate() 
f8 = open(adnotify_midlog, "wb")  
f8.truncate() 
f9 = open(adnotify_midlog, "wb")  
f9.truncate() 
syslog.info('begin to read apache log from %s' % hourlog)

for line in fileinput.input(hourlog):
    linecount +=1
    log_obj = logline(line)    

    dic_add(statuscount, log_obj.status) #统计requst状态
    if log_obj.status != '200' and log_obj.status != '302' and log_obj.status != '304' and log_obj.status != '206':
        syslog.info( 'http %s: %s' % (log_obj.status,   log_obj.rootpath))
    #todo: 需要改成contains的方式，否则会有
    if log_obj.rootpath == rootpathcollection.adlist_click:
        dic_add(adlist_clicks,log_obj.app_id )
        f.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.click:
        f2.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.mid_click:
        dic_add(mid_clicks,log_obj.app_id )
        f3.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.downok:
        dic_add(dones,log_obj.app_id )
        f4.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.connect:
        f5.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.give:
        f6.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.spend:
        f7.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.adnotify:
        f8.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.getinstalled:
        f9.write(log_obj.writeline())
        
    if rootpathcollection.all.has_key(log_obj.rootpath):
        dic_add(rootpatchcount, log_obj.rootpath);    
        if costcounts.has_key(log_obj.rootpath):
            costcounts[log_obj.rootpath].add(log_obj.cost)
        else:
            costcounts[log_obj.rootpath]=costcount(log_obj.rootpath)
    dic_add(channels, log_obj.channel_id)
    
f.close()
f2.close()
f3.close()
f4.close()
f5.close()
f6.close()
f7.close()
f8.close()
f9.close()
syslog.info( 'end of read apache log, total linecount: %s' % linecount)
syslog.info( 'begin to insert into database')

import MySQLdb
try:    
    conn = MySQLdb.connect(host='192.168.0.165', user='dian_user',passwd='dian~2011~2011@@', db='dianjoy')
    conn.autocommit(1)
    cursor = conn.cursor()
    for (k,v) in  costcounts.items():    
        sql = "insert into log_parser (stat_time,param,hit,avgtime,mintime,maxtime)values(CONCAT(CURDATE(),' ',CURTIME()),%s,%s,%s,%s,%s)"
        params = (k, v.hit, v.avgtime(), v.min, v.max)
        cursor.execute(sql,params)
    for (k,v) in  adlist_clicks.items():    
        sql = "insert into s_offer_adlist_stat(app_id,click_date,click_total)values(%s,%s,%s) ON DUPLICATE KEY update click_total=click_total+values(click_total)"#此处只能用%s，这是库的问题
        params = (k, currentdate, v)
        cursor.execute(sql,params)
    for (k,v) in  mid_clicks.items():    
        sql = "insert into s_offer_adview_stat(app_id,view_date,view_total)values(%s,%s,%s) ON DUPLICATE KEY update view_total=view_total+values(view_total)"#此处只能用%s，这是库的问题
        params = (k, currentdate, v)
        cursor.execute(sql,params)
    for (k,v) in  dones.items():    
        sql = "insert into s_offer_downok_stat(app_id,down_date,down_total)values(%s,%s,%s) ON DUPLICATE KEY update down_total=down_total+values(down_total)"#此处只能用%s，这是库的问题
        params = (k, currentdate, v)
        cursor.execute(sql,params)
    cursor.close()
    conn.close()
except Exception as e:
    syslog.error(str(e))
    sys.exit(0)

syslog.info('end of insert into database')
syslog.info('http status statistic:')
syslog.info(statuscount)
syslog.info(rootpatchcount)
error_count=0#http 404 and http 500
for (k,v) in  statuscount.items(): 
	if k == '404' or k == '500':
		error_count=error_count+v
if error_count>100:
	Tos = ['tech@dianjoy.com']
	send_mail('Alarm: 一小时apache日志中404和500数量大于100，请查验',Tos)
#2.7中才支持OrderedDict，所以只能注释掉
#print OrderedDict(sorted(rootpatchcount.items(), key=lambda t: t[1] , reverse = True))

#print 'remote host count: %s' % len(hots)
#print OrderedDict(sorted(rootpath.items(), key=lambda t: t[1] , reverse = True)) 
#print OrderedDict(sorted(apps.items(), key=lambda t: t[1] , reverse = True)) 
#print '#########channel stat##########'
#print channels

T2 = time.time()
syslog.info( 'time cost:')
syslog.info( T2-T1)
