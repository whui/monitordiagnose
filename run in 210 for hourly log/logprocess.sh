#!/bin/bash
day=`date -d"yesterday" +%Y-%m-%d`
grep adnotify.php /opt/lampp/logs/dianjoy.com-access_$day* | grep imsi | sort | cut -d' ' -f1,7 --output-delimiter='=' | cut -d'=' -f 1,3,4,5,10,12 --output-delimiter=' ' | sed 's/\&[a-z]*\(_[a-z]*\)*//g' > /opt/lampp/logs/backup/log_for_analysis.$day
python ip_cheat_analytics.py /opt/lampp/logs/backup/log_for_analysis.$day > /opt/lampp/htdocs/dev/upload/cheat_analysis/cheat_analysis.$day
gzip /opt/lampp/logs/dianjoy.com-access_$day*
