#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import fileinput
import sys 
import re 
import time 
import os
from apachelogparse import logline, log_to_dic, req_to_dic, rootpathcollection

#检查字典中是否存在此项，如存在则计数加1，不存在则插入
def dic_add(dic, key):
    #no need to add empty key
    if key == '' :
        return 
    if dic.has_key(key):
        dic[key]+=1
    else:
        dic[key]=1

#由IP的字典和另一个计数器组成的类
class hostcount:
    ips = {}
    linecount = 1
    def __init__(self,ip):
        self.ips = {}
        self.ips[ip] = 1   
    def addip(self, ip):
        self.ips[ip] = 1   
        self.linecount = self.linecount +1 
        
def dic_add_hostcount(dic, key,  host):
    if key == '':#no need to continue if app_id is empty
        return
    if dic.has_key(key):
        dic[key].addip(host)
    else:
        dic[key] = hostcount(host)

def dic_add_app_cid(dic, app_id, cid, host):
    tuplekey = (app_id, cid)
    if dic.has_key(tuplekey):
        dic[tuplekey].addip(host)
    else:
        dic[tuplekey] = hostcount(host)

#将给定的文件名（带路径）拆分成日期和小时
def split_fullfilepath_todatetime(line):
    filename = os.path.split(line)[1]#可以兼容不同OS的路径格式
    print filename
    filename = filename.replace('dianjoy.com-access_','')
    log_line_re = re.compile(r'''(?P<date>\d{4}-\d{2}-\d{2})#date
    -
    (?P<hour>\d{2})#hour
    .* #others
    ''', re.VERBOSE)
    m = log_line_re.match(filename)
    if m:
        groupdict = m.groupdict()
        return (groupdict['date'], groupdict['hour'])
    else:
        print "Input file name not match expected format "
        print "Expect:/opt/lampp/logs/dianjoy.com-access_2013-01-24-00"
        print "Actual:%s" % line

def initlog():
    import logging

    logger = logging.getLogger()
    code_file_path = os.path.abspath(os.path.dirname(sys.argv[0])) #代码文件所在目录
    hdlr = logging.FileHandler(os.path.join(code_file_path,'./apachelogprocessor%s.log' % \
                                            time.strftime('%Y-%m-%d',time.localtime(time.time()))))#在文件所在目录建log文件，一天之内用同一个
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    
    console = logging.StreamHandler()
    logger.addHandler(console)

    return logger
    
#主函数
syslog = initlog()
if len(sys.argv) <2:
    syslog.error('Argvs error,please input one arg: hourly apache log name(with path)')
    sys.exit()
#hourlog = 'C:\Users\HUI\Documents\python\TestData\dianjoy.com-access_2013-01-24-00-100000'
hourlog = sys.argv[1]
if not os.path.exists(hourlog):
    syslog.error('Input file %s not exist,please input valid log file' % hourlog)
    sys.exit()
    
T1 = time.time() #计时器
#从文件名获取日期和小时

datetimedic = split_fullfilepath_todatetime(hourlog)
currentdate = datetimedic[0]
currenthour = datetimedic[1]

#与数据库相关的四组统计数据
adlist_clicks = {} #t_adlist_click
mid_clicks = {} #t_mid_click_log
clicks = {} #t_click
dones = {} #t_downok

#其他重要统计数据
statuscount = {}
channels = {} #channel id stat
linecount = 0

#TODO: 此处需重构，新建一个mid_log建立类来构造file对象，现在的写法太繁琐
adclick_midlog = hourlog+'-adlist_midlog'
click_midlog = hourlog+'-click_midlog'
midclick_midlog = hourlog+'-midclick_midlog'
downok_midlog = hourlog+'-downok_midlog'
syslog.info('begin to creat mid_log files')
f = open(adclick_midlog, "wb")  
f.truncate() #清空原有内容
f2 = open(click_midlog, "wb")  
f2.truncate() 
f3 = open(midclick_midlog, "wb")  
f3.truncate() 
f4 = open(downok_midlog, "wb")  
f4.truncate() 
syslog.info('begin to read apache log from %s' % hourlog)

for line in fileinput.input(hourlog):
    linecount +=1
    log_obj = logline(line)    

    dic_add(statuscount, log_obj.status) #统计requst状态
    if log_obj.status != '200' and log_obj.status != '302' and log_obj.status != '304' and log_obj.status != '206':
        syslog.warning( 'http %s: %s' % (log_obj.status,   log_obj.rootpath))
        
    if log_obj.rootpath == rootpathcollection.adlist_click:
        dic_add_app_cid(adlist_clicks, log_obj.app_id, log_obj.channel_id, log_obj.ip)
        f.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.click:
        dic_add_app_cid(clicks, log_obj.app_id, log_obj.channel_id, log_obj.ip)
        f2.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.mid_click:
        f3.write(log_obj.writeline())
    if log_obj.rootpath == rootpathcollection.downok:
        f4.write(log_obj.writeline())
    
    dic_add(channels, log_obj.channel_id)
    
f.close()
f2.close()
f3.close()
f4.close()
syslog.info( 'end of read apache log, total linecount: %s' % linecount)
syslog.info( 'begin to insert into database')


import MySQLdb
try:    
    #conn = MySQLdb.connect(host='localhost', user='root',passwd='', db='dianjoy_new')
    conn = MySQLdb.connect(host='localhost', user='dian_user',passwd='dian~2011~2011@@', db='dianjoytest',unix_socket='/opt/lampp/var/mysql/mysql.sock')
    cursor = conn.cursor()
    #print '#########adlist_clicks##########'
    for (k,v) in  adlist_clicks.items():    
        sql = "insert into t_adlist_click_stat(appid,click_date,click_h,channel,nums_ip,nums_pv) \
        values(%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY update nums_ip=values(nums_ip),nums_pv=values(nums_pv)"#此处只能用%s，这是库的问题
        params = (k[0], currentdate, int(currenthour), k[1], len(v.ips), v.linecount)
        cursor.execute(sql,params)
        sql = "insert into t_adlist_click_stat(appid,click_date,click_h,channel,nums_ip,nums_pv) \
        values(%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY update nums_ip=nums_ip+values(nums_ip),nums_pv=nums_pv+values(nums_pv)"#此处只能用%s，这是库的问题
        params = (k[0], currentdate, 24, k[1], len(v.ips), v.linecount)
        cursor.execute(sql,params)
    #print '#########clicks##########'
    for (k,v) in  clicks.items():        
        sql = "insert into t_click_log_stat(appid,click_date,click_h,channel,nums_ip,nums_pv) \
        values(%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY update nums_ip=values(nums_ip),nums_pv=values(nums_pv)"#此处只能用%s，这是库的问题
        params = (k[0], currentdate, int(currenthour), k[1], len(v.ips), v.linecount)
        cursor.execute(sql,params)
        sql = "insert into t_click_log_stat(appid,click_date,click_h,channel,nums_ip,nums_pv) \
        values(%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY update nums_ip=nums_ip+values(nums_ip),nums_pv=nums_pv+values(nums_pv)"#此处只能用%s，这是库的问题
        params = (k[0], currentdate, 24, k[1], len(v.ips), v.linecount)
        cursor.execute(sql,params)
    cursor.close()
    conn.close()
except Exception as e:
    syslog.error(str(e))
    sys.exit(0)

syslog.info('end of insert into database')
syslog.info('http status statistic:')
syslog.info(statuscount)
#print 'remote host count: %s' % len(hots)
#print OrderedDict(sorted(rootpath.items(), key=lambda t: t[1] , reverse = True)) 
#print OrderedDict(sorted(apps.items(), key=lambda t: t[1] , reverse = True)) 
#print '#########channel stat##########'
#print channels

T2 = time.time()
syslog.info( 'time cost:')
syslog.info( T2-T1)
