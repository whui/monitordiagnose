#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import fileinput
import sys 
import re 
import time 
import os
from urlparse import urlparse

#将一行log拆分为字典
def log_to_dic(line):
    log_line_re = re.compile(r'''(?P<remote_host>\S+) #IP ADDRESS
    \s+ #whitespace
    \S+ #remote logname
    \s+ #whitespace
    \S+ #remote user
    \s+ #whitespace
    (?P<time>\[[^\[\]]+\]) #time
    \s+ #whitespace
    (?P<request>"[^"]+") #request
    \s+ #whitespace
    (?P<status>\d+)
    \s+ #whitespace
    (?P<bytes_sent>-|\d+)
    \s+ #whitespace
    \S+ #remote user #unkonw number
    \s+ #whitespace
    (?P<useragent>"[^"]+") #useragent of brower,os
    ''', re.VERBOSE)
    m = log_line_re.match(line)
    if m:
        groupdict = m.groupdict()
        return  groupdict
    else:
        print "this line not match log pattern: "
        print line
        return {}

#从[23/Jan/2013:23:59:59 +0800]这样的时间参数中获取小时数
def get_hour_from_log_time(timestr): #
    timestr_re = re.compile(r'''\[\d{2}/\S+/\d{4}: #day/month/year
    (?P<hour>\d{2}) #hour
    :\d{2}:\d{2}
    \s+ #whitespace
    \+\d{4}\] #m-second
    ''', re.VERBOSE)
    t = timestr_re.match(timestr)
    if t:
        groupdict = t.groupdict()
        return  groupdict['hour']
    else:
        return '00'
        

#将log中的request列拆分为url参数的字典
def req_to_dic(rawurl): #/dev/api/param.php?device_id=2
    url=urlparse('http://www.dianjoy.com'+rawurl)
    url_params={};
    for param in url.query.split('&'):
        #url_params[parm.split("=")[0]]=parm.split("=")[1]
        paramitems = param.split("=")
        if len(paramitems )==2:
            url_params[ param.split("=")[0] ] = param.split("=")[1]
    urlpathitems=url.path.split('/') #['', 'dev', 'api', 'user_account.php']
    if len(urlpathitems)>=4:
        url_params['mOS'] = urlpathitems[2]
        #url_params['rootpath'] = urlpathitems[3]
        url_params['rootpath'] = url.path

    return url_params


class rootpathcollection:
    adlist_click = '/dev/api/adlist/adlist.php'
    mid_click = '/dev/api/adlist/adview.php'
    click = '/dev/api/adlist/download.php'
    downok = '/dev/api/downok.php'

class logline:
    #basic params   
    ip = ''
    status = ''
    
#    hour =''
    
    
    #url params
    rootpath =''
    app_id = ''
    device_id = ''
    ad_id =''
    channel_id = 'none' #set default value to 'none'，有很多channel没有，但是也应该加入统计，因为此项并不重要
    url_params = {}
    
    net = '' # wifi or 3G
    imsi = '' #sim卡编号
    sdkv = '' #sdkversion
    app_package_name = ''
    screen = '' #800*600
    
    #TODO：以下各项，如果url中没有，从user agent中获取，则肯定有空格
    os_type = '' # Android 
    os_version = '' #4.1.1 
    device_name =''#SOAYE-W9
    
    
    #if log line valid
    valid = 0
    def __init__(self,line):
        items = log_to_dic(line)
        if len(items) == 0: #为0是正则匹配失败，也就是拆分失败
            return
        
        self.valid = 1    
        if items.has_key('remote_host'):
            self.ip =  items['remote_host'] 
        else:
            self.ip =  ''
        self.status =  items['status'] if items.has_key('status') else '' #http status, default value : 200
        if len( items['request'].split()) >1 :
            self.url_params = req_to_dic(items['request'].split()[1] )
        
#        logtime = items['time'] if items.has_key('time') else ''
#        self.hour =  get_hour_from_log_time(logtime)
        
        self.rootpath = self.url_params['rootpath'] if self.url_params.has_key('rootpath') else ''
        if self.rootpath == '': #no need to continue if rootpath is empty
            return
        
        #以下每一项都很可能不存在，应该加入判断，没有则留为空
        self.app_id = self.url_params['app_id'] if self.url_params.has_key('app_id') else ''
        if self.app_id == '':#sometimes parameter name is appid
            self.app_id = self.url_params['appid'] if self.url_params.has_key('appid') else ''
            
        self.device_id = self.url_params['device_id'] if self.url_params.has_key('device_id') else ''
        if self.device_id == '':#sometimes parameter name is deviceid
            self.device_id = self.url_params['deviceid'] if self.url_params.has_key('deviceid') else ''
            
        self.ad_id = self.url_params['ad_id'] if self.url_params.has_key('ad_id') else ''
        if self.ad_id == '':#sometimes parameter name is adid
            self.ad_id = self.url_params['adid'] if self.url_params.has_key('adid') else ''
                    
        self.channel_id = self.url_params['cid'] if self.url_params.has_key('cid') else 'none'
        if self.channel_id == 'none':#sometimes parameter name is channel_id
            self.channel_id = self.url_params['channel_id'] if self.url_params.has_key('channel_id') else 'none'        
        self.net = self.url_params['net'] if self.url_params.has_key('net') else ''
        self.imsi = self.url_params['imsi'] if self.url_params.has_key('imsi') else ''
        self.sdkv = self.url_params['lv'] if self.url_params.has_key('lv') else ''
        if self.sdkv == '':#sometimes parameter name is library_version
            self.sdkv = self.url_params['library_version'] if self.url_params.has_key('library_version') else ''         
        self.app_package_name = self.url_params['app_package_name'] if self.url_params.has_key('app_package_name') else '' 
        #screen solution
        width = ''
        height = ''
        if self.url_params.has_key('screen_width') :
            width = str(self.url_params['screen_width'])
        if self.url_params.has_key('screen_height') :
            height = str(self.url_params['screen_height'])
        self.screen = ''.join([width,  '-' , height])
        
        #os, device_name should be taken from user agent
        self.os = ''
        self.device_name=''
            
    def writeline(self):
        #用\t不用空格因为这些内容可能有空格，如os type        
        #return '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s%s' % (self.ad_id, self.app_id, self.channel_id, self.device_id, self.ip, self.rootpath, self.status,  self.net,self.imsi,self.sdkv,self.app_package_name, self.os,self.device_name,'\n')
        return '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s%s' % \
        (self.ad_id, self.app_id, self.channel_id, self.device_id, self.ip, self.rootpath ,self.status,self.net,self.imsi,self.sdkv,self.app_package_name,self.screen,'\n')
    def is_valid_request(self):
        #'200'  #正常访问  '302' #重定向 '304' #访问了缓存，常见于css和图片  '206': #部分访问，常见于apk文件
        if self.status == '200' or self.status == '302' or self.status == '304' or self.status == '206': 
            return true
        else:
            return false
            
    def is_valid_logline(self):
        return self.valid 
