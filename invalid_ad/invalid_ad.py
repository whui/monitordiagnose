#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import sys 
import os
import MySQLdb
from email_sender import send_mail, send_mail_with_text
    
#主函数
Tos = ['business@dianjoy.com', 'tech@dianjoy.com']
#Tos = ['hui.wang@dianjoy.com']
try:    
    conn = MySQLdb.connect(host='192.168.0.165', user='dian_user',passwd='dian~2011~2011@@', db='dianjoy')
    cursor = conn.cursor()
    sql = "SELECT ad_id, COUNT('X') AS num FROM t_offer_downok_log WHERE `ctime` > DATE_ADD(NOW(), INTERVAL -1 HOUR) GROUP BY ad_id HAVING num>50"
    cursor.execute(sql)
    downok_res= cursor.fetchall()
    
    sql = "SELECT ad_id, COUNT('X') AS num FROM `t_offer_transfer_log` WHERE `transfer_time` > DATE_ADD(NOW(), INTERVAL -1 HOUR) GROUP BY ad_id;"
    cursor.execute(sql)
    transfer_res= cursor.fetchall()

    print downok_res
    print transfer_res
    
    invalid_ads = []
    for res in downok_res:
        ad_id = res[0]
        if ad_id == 'null':
            continue
        downook = res[1]
        transfer = 0
        for t_res in transfer_res:
            if t_res[0]==ad_id:
                transfer = t_res[1]
        if transfer*100/downook < 25:
            invalid_ads.append("[ad_id: %s, downook: %s, transfer: %s]" % (ad_id, downook, transfer))

    cursor.close()
    conn.close()
    
    if len(invalid_ads)>0:
        send_mail_with_text('Alarm: 以下广告在过去一小时中下载完成大于50，但激活数远小于下载完成数',Tos,  ",".join(invalid_ads))

    
except Exception as e:
    send_mail('Alarm: can`t connect to database when trying to query for ads` transfer/downok number',Tos)
