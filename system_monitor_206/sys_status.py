#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import os 
import re,time 
from email_sender import send_mail 

def load_stat(): 
    loadavg = {} 
    f = open("/proc/loadavg") 
    con = f.read().split() 
    f.close() 
    loadavg['lavg_1']=con[0] 
    loadavg['lavg_5']=con[1] 
    loadavg['lavg_15']=con[2] 
    loadavg['nr']=con[3] 
    loadavg['last_pid']=con[4] 
    return loadavg 
def memory_stat():   
    mem = {}   
    f = open("/proc/meminfo") 
    lines = f.readlines() 
    f.close() 
    for line in lines: 
        if len(line) < 2: continue 
        name = line.split(':')[0] 
        var = line.split(':')[1].split()[0] 
        mem[name] =  long(var) /1024 
    mem['MemUsed'] = mem['MemTotal'] - mem['MemFree'] - mem['Buffers'] - mem['Cached'] 
    mem['FreePecent'] = (mem['MemFree']+ mem['Buffers']+mem['Cached'])*100/mem['MemTotal'] 
    return mem 
 
def disk_stat(): 
    import os 
    hd={} 
    disk = os.statvfs("/") 
#    print disk 
    hd['available'] = disk.f_bsize * disk.f_bavail / 1024000 
    hd['capacity'] = disk.f_bsize * disk.f_blocks /1024000 
    hd['Free'] = disk.f_bsize * disk.f_bfree /1024000 
    hd['FreePecent'] = hd['Free']*100 /hd['capacity']
    return hd 
#print disk_stat() 

def _read_cpu_usage(): 
        statfile = "/proc/stat" 
        cpulist = [] 
        try: 
            f = open(statfile, 'r') 
            lines = f.readlines() 
        except: 
            print "error:无法打开文件%s，系统无法继续运行。" % (statfile) 
            return [] 
        for line in lines: 
            tmplist = line.split() 
            if len(tmplist) < 5: 
                continue 
            for b in tmplist: 
                m = re.search(r'cpu\d+',b) 
                if m is not None: 
                    cpulist.append(tmplist) 
        f.close() 
        return cpulist 

def get_cpu_usage(): 
        cpuusage = {} 
        cpustart = {} 
        cpuend = {} 
        linestart = _read_cpu_usage() 
        if not linestart: 
            return 0 
        for cpustr in linestart: 
            usni=long(cpustr[1])+long(cpustr[2])+long(cpustr[3])+long(cpustr[5])+long(cpustr[6])+long(cpustr[7])+long(cpustr[4]) 
            usn=long(cpustr[1])+long(cpustr[2])+long(cpustr[3]) 
            cpustart[cpustr[0]] = str(usni)+":"+str(usn) 
        sleep = 2 
        time.sleep(sleep) 
        lineend = _read_cpu_usage() 
        if not lineend: 
            return 0 
        for cpustr in lineend: 
            usni=long(cpustr[1])+long(cpustr[2])+long(cpustr[3])+long(cpustr[5])+long(cpustr[6])+long(cpustr[7])+long(cpustr[4]) 
            usn=long(cpustr[1])+long(cpustr[2])+long(cpustr[3]) 
            cpuend[cpustr[0]] = str(usni)+":"+str(usn) 
        for line in cpustart: 
            start = cpustart[line].split(':') 
            usni1,usn1 = float(start[0]),float(start[1]) 
            end = cpuend[line].split(':') 
            usni2,usn2 = float(end[0]),float(end[1]) 
            cpuper=(usn2-usn1)/(usni2-usni1) 
            cpuusage[line] = int(100*cpuper) 
         
        return cpuusage 
        

Tos = ['tech@dianjoy.com']
Target = '221.174.25.206'
MemTotal = memory_stat()['MemTotal']
FreePecentMem = memory_stat()['FreePecent']
HDTotal = disk_stat()['capacity']
FreePecentHD = disk_stat()['FreePecent']
print "MemTotal %s, Free Pecent %s%%, Hard Disk Total %sM, Hard Disk Free Pecent %s%%" % (MemTotal, FreePecentMem, HDTotal, FreePecentHD)
if FreePecentMem<20:
    send_mail('Alarm for %s: Free memory percent low to: %s %%' % ( Target,FreePecentMem ) ,Tos)
if FreePecentHD<10:
    send_mail('Alarm for %s: Free hard disk space percent low to: %s %%' % ( Target,FreePecentHD ) ,Tos) 
cpuusedarr = get_cpu_usage()
print cpuusedarr
total_pecent = 0
for key, value in cpuusedarr.items():
	total_pecent = total_pecent+value
print total_pecent/len(cpuusedarr.items())
if total_pecent/len(cpuusedarr.items()) > 80:
    send_mail('Alarm for %s: avg cpu used ratio > 0.8, cpus ratios: %s' % ( Target,cpuusedarr ) ,Tos) 