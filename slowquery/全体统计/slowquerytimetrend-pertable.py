#_*_encoding:utf-8_*_
#解决源文件不兼容中文的问题
import fileinput
import sys 
import re 
import time 
import os


#主函数

if len(sys.argv) <2:
    print'Argvs error,please input one arg: hourly apache log name(with path)'
    sys.exit()
#hourlog = 'C:\Users\HUI\Documents\Sql\dianjoy-slow-100k-simple.log'
hourlog = sys.argv[1]
tablename = sys.argv[2]
timelog = hourlog+'-time-'+tablename
f = open(timelog, "wb")  
f.truncate() #清空原有内容
if not os.path.exists(hourlog):
    print 'Input file %s not exist,please input valid log file' % hourlog
    sys.exit()
    
ctime = '00:00:00'
qtime = 0
ltime = 0
count = 0

T1 = time.time() #计时器
log_line_t = re.compile(r'''# Time: \d+\s{1,2}(?P<time>\S+)''')
log_line_q = re.compile(r'''# Query_time: (?P<qtime>\S+)  Lock_time: (?P<ltime>\S+) ''')
for line in fileinput.input(hourlog):
    m = log_line_t.match(line)
    if m:
        timeitems = m.groupdict()['time'].split(':')
        if len(timeitems) == 3 :
            minutetime = timeitems[0] + ":" + timeitems[1] +":00"
    n = log_line_q.match(line)
    if n:
        qtime = qtime + float(n.groupdict()['qtime'])
        ltime = ltime + float(n.groupdict()['ltime'])
        count = 1
        #qtime = qtime + 1
    try: #这里的try因为有非utf - 8 字符导致会出错
        if line.find(tablename)>0:
            f.write('%s\t%s\t%s\t%s\n' % (minutetime, qtime, ltime, count))
            ctime = minutetime
            qtime = 0
            ltime = 0
            count = 0
    except:
        continue
f.close()

T2 = time.time() #计时器
print 'time cost:'
print T2-T1
