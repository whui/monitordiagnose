#_*_encoding:utf-8_*_
import fileinput
import sys 
import re 
import time 
import os
from collections import OrderedDict

def dic_add(dic, key):    
    if dic.has_key(key):
        dic[key]+=1
    else:
        dic[key]=1
        

def dic_add_tup(dic, id_key, deviceid):
    tuplekey = (id_key, deviceid)
    dic_add(dic, tuplekey)
    
def log_to_dic(line):
    log_line_re = re.compile(r'''select pack_name from t_setup where id_key='(?P<id_key>\S+)' and deviceid='(?P<deviceid>\S+)'.*;''')
    m = log_line_re.match(line)
    if m:
        groupdict = m.groupdict()
        return  groupdict
    else:
        print "this line not match log pattern: "
        print line
        return {}

hourlog = r'C:\Users\HUI\Documents\python\workingitem\slowquery\tmp1.log'
    
T1 = time.time() 

result = {}

for line in fileinput.input(hourlog):
    items = log_to_dic(line)  
    if len(items) == 0:
        continue;
    id_key = items["id_key"]
    deviceid = items["deviceid"]
    dic_add_tup(result, id_key, deviceid)

#print OrderedDict(sorted(result.items(), key=lambda t: t[1] , reverse = True)) 
for k in result:
    if result[k]>20:
        print "%s,%s" % k, result[k]

T2 = time.time()
print 'time cost:'
print T2-T1
